// Copyright Epic Games, Inc. All Rights Reserved.

#include "WinterIsComing.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE(FDefaultGameModuleImpl, WinterIsComing, "WinterIsComing");

DEFINE_LOG_CATEGORY(LogFlying)
