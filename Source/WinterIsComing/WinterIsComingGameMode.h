// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"

#include "Engine/Texture2D.h"

#include "WinterIsComingGameMode.generated.h"

UCLASS(MinimalAPI)
class AWinterIsComingGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	AWinterIsComingGameMode();
	UFUNCTION(BlueprintNativeEvent)
	void End();
	void End_Implementation();
	UFUNCTION(BlueprintNativeEvent)
	void PlayFlipbook(UTexture2D* Texture);
	void PlayFlipbook_Implementation(UTexture2D* Texture);
};



