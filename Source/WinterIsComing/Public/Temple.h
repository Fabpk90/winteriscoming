// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Capturable.h"

#include "CodexAsset.h"
#include "Components/AudioComponent.h"

#include "Temple.generated.h"

UCLASS()
class WINTERISCOMING_API ATemple : public ACapturable
{
	GENERATED_BODY()

private:
	int NbSubscribed;
	int NbCaptured;
public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	bool LastTemple;
	bool Captured;
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	UCodexAsset* Codex;

public:	
	// Sets default values for this actor's properties
	ATemple();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:
	virtual void Capture_Implementation();

	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UFUNCTION(BlueprintNativeEvent)
	void TotemCaptured(int nbCaptured);
	void TotemCaptured_Implementation(int nbCaptured);

	void AddSubscribed();
	void AddCaptured();
};
