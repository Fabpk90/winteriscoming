// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Capturable.h"

#include "Temple.h"
#include "GameFramework/Pawn.h"

#include "Components/SphereComponent.h"

#include "Totem.generated.h"

UCLASS()
class WINTERISCOMING_API ATotem : public ACapturable
{
	GENERATED_BODY()
	
private:
	float CurrentAngle;
	float AngleSum;
	FVector LastKnownPosition;
	FVector EnterPosition;
	APawn* Visitor;
protected:
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	USphereComponent* SphereTrigger;
public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	ATemple* Temple;
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	float DetectionRadius;
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	float AngleToCapture;

public:
	// Sets default values for this actor's properties
	ATotem();

private:
	void CheckForAngle();
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	virtual void Capture_Implementation() override;
	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UFUNCTION()
	void OnSphereBeginOverlap(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);
	UFUNCTION()
	void OnSphereEndOverlap(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex);
	//Edition
	//virtual void PostEditChangeProperty(FPropertyChangedEvent &PropertyChangedEvent) override;

};
