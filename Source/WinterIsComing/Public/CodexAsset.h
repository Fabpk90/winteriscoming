// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

#include "Sound/SoundCue.h"

#include "Engine/DataAsset.h"
#include "Engine/Texture2D.h"

#include "CodexAsset.generated.h"

/**
 * 
 */
UCLASS(ClassGroup = Codex, Category = "Codex", BlueprintType)
class WINTERISCOMING_API UCodexAsset : public UDataAsset
{
	GENERATED_BODY()
public:
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
		USoundCue* SoundCue;
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
		UTexture2D* Texture;
};
