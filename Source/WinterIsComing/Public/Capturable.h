// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

#include "Components/AudioComponent.h"

#include "GameFramework/Actor.h"
#include "Capturable.generated.h"

UCLASS()
class WINTERISCOMING_API ACapturable : public AActor
{
	GENERATED_BODY()

private:

protected:

	bool Captured;

public:
	UPROPERTY(BlueprintReadWrite)
	UAudioComponent* AudioComponent;

public:
	// Sets default values for this actor's properties
	ACapturable();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:
	virtual bool TryCapture();
	UFUNCTION(BlueprintNativeEvent)
	void Capture();
	virtual void Capture_Implementation();

	// Called every frame
	virtual void Tick(float DeltaTime) override;

	//Accessors
	virtual bool IsCaptured() { return Captured; }
};
