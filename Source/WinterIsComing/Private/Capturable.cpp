// Fill out your copyright notice in the Description page of Project Settings.


#include "Capturable.h"

// Sets default values
ACapturable::ACapturable()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	AudioComponent = CreateDefaultSubobject<UAudioComponent>(TEXT("AudioSource"));
	SetRootComponent(AudioComponent);
}

// Called when the game starts or when spawned
void ACapturable::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ACapturable::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

bool ACapturable::TryCapture()
{
	return !Captured;
}

void ACapturable::Capture_Implementation()
{
	//UE_LOG(LogTemp, Warning, TEXT("Capture_implementation"));
	Captured = true;
}

