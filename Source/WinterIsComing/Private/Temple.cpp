// Fill out your copyright notice in the Description page of Project Settings.


#include "Temple.h"
#include "../WinterIsComingGameMode.h"

// Sets default values
ATemple::ATemple()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
}

// Called when the game starts or when spawned
void ATemple::BeginPlay()
{
	Super::BeginPlay();
}

void ATemple::Capture_Implementation()
{
	//UE_LOG(LogTemp, Warning, TEXT("HELLO %d"), NbSubscribed);
	Captured = true;
	AWinterIsComingGameMode* GM = (AWinterIsComingGameMode*)GetWorld()->GetAuthGameMode();
	if (Codex)
	{
		GM->PlayFlipbook(Codex->Texture);
	}

	if (LastTemple)
	{
		GM->End();
	}
}

// Called every frame
void ATemple::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ATemple::TotemCaptured_Implementation(int nbCaptured)
{

}

void ATemple::AddSubscribed()
{
	++NbSubscribed;
	UE_LOG(LogTemp, Warning, TEXT("Adding subscribed, up to %d"), NbSubscribed);
}

void ATemple::AddCaptured()
{
	if (NbCaptured < NbSubscribed)
	{
		++NbCaptured;
		TotemCaptured(NbCaptured);
		if (TryCapture())
		{
			Capture();
		}
	}
	UE_LOG(LogTemp, Warning, TEXT("Adding captured, up to %d"), NbSubscribed);
}

