// Fill out your copyright notice in the Description page of Project Settings.


#include "Totem.h"
#include "Math/UnrealMathUtility.h"
#include "Kismet/KismetMathLibrary.h"
#include "DrawDebugHelpers.h"

// Sets default values
ATotem::ATotem()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	SphereTrigger = CreateDefaultSubobject<USphereComponent>(TEXT("SphereTrigger"));
	SphereTrigger->SetupAttachment(GetRootComponent());
	SphereTrigger->OnComponentBeginOverlap.AddDynamic(this, &ATotem::OnSphereBeginOverlap);
	SphereTrigger->OnComponentEndOverlap.AddDynamic(this, &ATotem::OnSphereEndOverlap);

	AngleSum = 0.0f;
}

void ATotem::CheckForAngle()
{
	FVector DirToEnterPos = (GetActorLocation() - EnterPosition);
	DirToEnterPos.Normalize();

	FVector DirToPreviousPos = (GetActorLocation() - LastKnownPosition);
	DirToPreviousPos.Normalize();

	FVector DirToCurrentPos = (GetActorLocation() - Visitor->GetActorLocation());
	DirToCurrentPos.Normalize();



	//float TotalAngle = UKismetMathLibrary::Abs(FMath::RadiansToDegrees(FVector::DotProduct(DirToCurrentPos, DirToEnterPos)));
	float DeltaAngle = FMath::RadiansToDegrees(FMath::Abs(FMath::Acos(FVector::DotProduct(DirToCurrentPos, DirToPreviousPos))));

	//UE_LOG(LogTemp, Warning, TEXT("BITE %f"), AngleSum);

	AngleSum += DeltaAngle;

	if (AngleSum >= AngleToCapture && TryCapture())
	{
		this->Capture();
	}

	LastKnownPosition = Visitor->GetActorLocation();
}

// Called when the game starts or when spawned
void ATotem::BeginPlay()
{
	Super::BeginPlay();
	//Subscribing to potential existing Temple
	if (Temple)
	{
		Temple->AddSubscribed();
	}
}

void ATotem::Capture_Implementation()
{
	if (Temple && !Captured)
	{
		UE_LOG(LogTemp, Warning, TEXT("Captured"));
		Captured = true;
		Temple->AddCaptured();
	}
}

// Called every frame
void ATotem::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	if (!Captured && Visitor)
	{
		CheckForAngle();
	}
}

void ATotem::OnSphereBeginOverlap(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	//UE_LOG(LogTemp, Warning, TEXT("Name : %s"), *(OtherActor->GetFName().ToString()));
	if (!Visitor)
	{
		Visitor = Cast<APawn>(OtherActor);
		if (Visitor)
		{
			EnterPosition = Visitor->GetActorLocation();
			LastKnownPosition = EnterPosition;
		}
	}
}

void ATotem::OnSphereEndOverlap(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
	UE_LOG(LogTemp, Warning, TEXT("END OVERLAP"));
	if (Visitor)
	{
		Visitor = NULL;
		CurrentAngle = 0.f;
	}
}

