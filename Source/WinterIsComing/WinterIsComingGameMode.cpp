// Copyright Epic Games, Inc. All Rights Reserved.

#include "WinterIsComingGameMode.h"
#include "WinterIsComingPawn.h"

AWinterIsComingGameMode::AWinterIsComingGameMode()
{
	// set default pawn class to our flying pawn
	DefaultPawnClass = AWinterIsComingPawn::StaticClass();
}
void AWinterIsComingGameMode::End_Implementation()
{

}
void AWinterIsComingGameMode::PlayFlipbook_Implementation(UTexture2D* Texture)
{

}
