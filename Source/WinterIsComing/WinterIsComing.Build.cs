// Copyright Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;

public class WinterIsComing : ModuleRules
{
	public WinterIsComing(ReadOnlyTargetRules Target) : base(Target)
	{
		PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;

		PublicDependencyModuleNames.AddRange(new string[] { "Core", "CoreUObject", "Engine", "InputCore" });
	}
}
